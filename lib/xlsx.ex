defmodule Xlsx do
 

  def main do
    #read_file("/home/dvidvlencia/Documentos/test/xlsx/datos.xlsx")
    #|> get_sheet

    read_file("/home/dvidvlencia/Documentos/test/xlsx/datos.xlsx")
    |> extract_column
  end

  def read_file(path) do
    Xlsxir.multi_extract(path, 0, false, nil, [])
    #Xlsxir.peek(path, 0, 100, [])
    #Xlsxir.stream_list(path, 0, [])
  end


   defp get_sheet({:ok, table_id}) do
    IO.puts "------>"
    #IO.inspect(table_id)
    Xlsxir.get_map(table_id)
    #Enum.member?(:ets.all, table_id)
  end

  def parallel_parsing do
    #task2 = Task.async(fn -> Xlsxir.extract("/home/dvidvlencia/Documentos/test/xlsx/datos.xlsx", 0) end)
    #{:ok, tid2} = Task.await(task2)
    #Xlsxir.get_list(tid2)
    
    task1 = Task.async(fn -> Xlsxir.extract("/home/dvidvlencia/Documentos/test/xlsx/datos.xlsx", 0) end)
    {:ok, tid1} = Task.await(task1)
    IO.inspect Xlsxir.get_map(tid1)
    IO.inspect Xlsxir.get_list(tid1)
    [head | tail] = Xlsxir.get_list(tid1)
    head
     
  end

  def extract_column({:ok, table_id}) do
    # TODO: replace string with dynamic variable for column
    Xlsxir.get_col(table_id, "A")

  end


  def read_rtf do 
    File.read(Path.absname("./") <> "test.txt")
    #{:ok, File.read(Path.absname("./") <> "test.rtf")}
  end
  

end
